

#pragma once
#ifndef MAP_TRANSITION_STATE
#define MAP_TRANSITION_STATE

#include <iostream>
#include <vector>
#include "MenuState.h"
#include "TextureManager.h"
#include "Game.h"
#include "PlayState.h"
class GameObject;

class MapTransitionState : public MenuState
{
public:

	virtual ~MapTransitionState() {}

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_transitionID; }

	virtual void setCallbacks(const std::vector<Callback>& callbacks);

private:

	static const std::string s_transitionID;

	std::vector<GameObject*> m_gameObjects;
};


#endif
