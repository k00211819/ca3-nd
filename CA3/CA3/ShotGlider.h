//
//  ShotGlider.h
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 30/03/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#ifndef SDL_Game_Programming_Book_ShotGlider_h
#define SDL_Game_Programming_Book_ShotGlider_h

#include "Glider.h"

class ShotGlider : public Glider
{
public:
    
	virtual ~ShotGlider() { std::cout << "Shot deleted " << std::endl; }
    
    ShotGlider() : Glider()
    {
        m_bulletFiringSpeed = 100;
        m_moveSpeed = -1;

		m_health = 10;
    }
    
    virtual void load(std::unique_ptr<LoaderParams> const &pParams)
    {
        ShooterObject::load(std::move(pParams));

		m_velocity.setY(0);
		m_velocity.setX(m_moveSpeed);
    }

	virtual void collision()
	{
		m_health -= 1;
		TheSoundManager::Instance()->playSound("explode", 0);

		m_moveSpeed-= 0.2;

		m_velocity.setX(m_moveSpeed);

		TheGame::Instance()->increaseEnemiesKilled();

		int max = 800;
		int min = 100;
		int randNum = rand() % (max - min + 1) + min;

		m_position.setY(randNum);
		m_position.setX(900);

		if (m_health == 0)
		{
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "explosion";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 40;
				m_height = 40;
				m_bDying = true;
			}
		}
	}

    virtual void update()
    {
        if(!m_bDying)
        {
			if (m_position.getX() < 100)
			{

				int max = 800;
				int min = 100;
				int randNum = rand() % (max - min + 1) + min;

				m_position.setY(randNum);
				m_position.setX(900);
			}
        }
        else
        {
            m_velocity.setX(0);
            doDyingAnimation();
        }
        
        ShooterObject::update();
    }
};

class ShotGliderCreator : public BaseCreator
{
    GameObject* createGameObject() const
    {
        return new ShotGlider();
    }
};


#endif
