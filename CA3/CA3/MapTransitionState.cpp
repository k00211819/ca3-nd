#pragma once
#include "MapTransitionState.h"

const std::string MapTransitionState::s_transitionID = "Map_TRANSITION";

void MapTransitionState::update()
{
	if (m_loadingComplete && !m_exiting)
	{
		TheGame::Instance()->getStateMachine()->changeState(new PlayState());
	}
}

void MapTransitionState::render()
{
	if (m_loadingComplete)
	{
		
	}
}

bool MapTransitionState::onEnter()
{
	
	m_loadingComplete = true;
	std::cout << "Entering MaptransitionState\n";
	return true;
}

bool MapTransitionState::onExit()
{
	m_exiting = true;

	std::cout << "Exiting MaptransitionState\n";

	TheTextureManager::Instance()->clearTextureMap();

	return true;
}

void MapTransitionState::setCallbacks(const std::vector<Callback>& callbacks)
{
}