//
//  PlayState.cpp
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 09/02/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#include <iostream>
#include "PlayState.h"
#include "GameOverState.h"
#include "PauseState.h"
#include "Game.h"
#include "InputHandler.h"
#include "LevelParser.h"
#include "Level.h"
#include "BulletHandler.h"

const std::string PlayState::s_playID = "PLAY";

void PlayState::update()
{
    if(m_loadingComplete && !m_exiting)
    {
        if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_ESCAPE))
        {
            TheGame::Instance()->getStateMachine()->pushState(new PauseState());
        }

		// If there are no more enemies left to kill
		if (TheGame::Instance()->getEnemiesLeftToKill() <= 0)
		{
			TheGame::Instance()->setLevelComplete(true);
		}

        
        TheBulletHandler::Instance()->updateBullets();
        
        if(TheGame::Instance()->getPlayerLives() == 0)
        {
            TheGame::Instance()->getStateMachine()->changeState(new GameOverState());
        }
        
        if(pLevel != 0)
        {
            pLevel->update();
        }
    }
}

void PlayState::render()
{
    if(m_loadingComplete)
    {
        if(pLevel != 0)
        {
            pLevel->render();
        }
        
        for(int i = 0; i < TheGame::Instance()->getPlayerLives(); i++)
        {
            TheTextureManager::Instance()->drawFrame("lives", 10 +i * 30, 20, 32, 30, 0, 0, TheGame::Instance()->getRenderer(), 300, 255);
        }
        
        TheBulletHandler::Instance()->drawBullets();

		TheTextureManager::Instance()->printText("Enemies destroyed:  " + std::to_string(TheGame::Instance()->m_enemiesKilled), 120, 10, 0, 0, TheGame::Instance()->getRenderer());
		TheTextureManager::Instance()->printText("To next level:            " + std::to_string(TheGame::Instance()->getEnemiesLeftToKill()), 121, 44, 0, 0, TheGame::Instance()->getRenderer());

		if (TheGame::Instance()->getLevelComplete() && TheGame::Instance()->getCurrentLevel() == 1)
		{
			TheTextureManager::Instance()->printText("You have completed level 1.", 312, 300, 0, 0, TheGame::Instance()->getRenderer());
		}
		else if (TheGame::Instance()->getLevelComplete() && TheGame::Instance()->getCurrentLevel() == 2)
		{
			TheTextureManager::Instance()->printText("You have completed level 2.", 312, 300, 0, 0, TheGame::Instance()->getRenderer());
		}
		else if (TheGame::Instance()->getLevelComplete() && TheGame::Instance()->getCurrentLevel() == 3)
		{
			TheTextureManager::Instance()->printText("You have completed the video game.", 312, 300, 0, 0, TheGame::Instance()->getRenderer());
		}
    }
}

bool PlayState::onEnter()
{
    TheGame::Instance()->setPlayerLives(3);
    
    LevelParser levelParser;
    pLevel = levelParser.parseLevel(TheGame::Instance()->getLevelFiles()[TheGame::Instance()->getCurrentLevel() - 1].c_str());

    
    TheTextureManager::Instance()->load("assets/bullet1.png", "bullet1", TheGame::Instance()->getRenderer());
    TheTextureManager::Instance()->load("assets/bullet2.png", "bullet2", TheGame::Instance()->getRenderer());
    TheTextureManager::Instance()->load("assets/bullet3.png", "bullet3", TheGame::Instance()->getRenderer());
    TheTextureManager::Instance()->load("assets/lives.png", "lives", TheGame::Instance()->getRenderer());

	
    
    if(pLevel != 0)
    {
        m_loadingComplete = true;
    }
    
    std::cout << "entering PlayState\n";
    return true;
}

bool PlayState::onExit()
{
    m_exiting = true;
    
    TheInputHandler::Instance()->reset();
    TheBulletHandler::Instance()->clearBullets();
    
    std::cout << "exiting PlayState\n";
    return true;
}
